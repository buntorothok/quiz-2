CREATE TABLE customers(
    id INT AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    password varchar(255),
    PRIMARY KEY(id)
);

CREATE TABLE orders(
    id INT AUTO_INCREMENT,
    amount INT,
    customer_id INT,
    PRIMARY KEY(id),
    FOREIGN KEY (customer_id) REFERENCES customers(id)
);